//
//  CarViewController.swift
//  17_10_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 24.10.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

class CarViewController: UIViewController {

    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var resultTimeLabel: UILabel!
    @IBOutlet weak var distanceTextFead: UITextField!
    
    let car = Car(speed: 0, maxSpeed: 250)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateSpeed()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func resultButtonTouch(_ sender: Any) {
        if let distance = Int(distanceTextFead.text ?? "") {
            resultTimeLabel.text = "\(car.getTimeForThatDistance(distance: distance)) h"
        }
        
    }
    @IBAction func upSpeedButtonTouch(_ sender: Any) {
        car.upSpeed(speedToUp: 20)
        updateSpeed()
    }
    @IBAction func downSpeedButtonTouch(_ sender: Any) {
        car.downSpeed(speedToDown: 40)
        updateSpeed()
    }
    
    func updateSpeed() {
        speedLabel.text = "SPEED : \(car.speed)"
    }
    
}
