//
//  Car.swift
//  17_10_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 24.10.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import Foundation

class Car {
    private(set) var speed: Int
    private let maxSpeed: Int
    init(speed: Int, maxSpeed: Int) {
        self.speed = speed
        self.maxSpeed = maxSpeed
    }
    func upSpeed(speedToUp: Int) {
        self.speed = min(speedToUp + self.speed, maxSpeed)
    }
    func downSpeed(speedToDown: Int) {
        self.speed = max(self.speed - speedToDown, 0)
    }
    func getTimeForThatDistance(distance: Int) -> Double {
        return Double(Double(distance) / Double(self.speed))
    }
}
