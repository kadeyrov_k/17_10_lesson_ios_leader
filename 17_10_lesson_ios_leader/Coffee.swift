//
//  Coffee.swift
//  17_10_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 17.10.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import Foundation

enum Coffee: String {
    case blackCoffee = "Black Coffee"
    case latte = "Latte"
    case bigCoffee = "Big Coffee"
    case hugeCoffee = "Huge Coffee"
    
    var coffeeNeed: Int {
        switch self {
        case .bigCoffee:
            return 30
        case .latte:
            return 20
        case .blackCoffee:
            return 10
        case .hugeCoffee:
            return 40
        }
    }
    
    var waterNeed: Int {
        switch self {
        case .bigCoffee:
            return 300
        case .latte:
            return 250
        case .blackCoffee:
            return 170
        case .hugeCoffee:
            return 500
        }
    }
    
    var milkNeed: Int {
        switch self {
        case .bigCoffee:
            return 100
        case .latte:
            return 70
        case .blackCoffee:
            return 0
        case .hugeCoffee:
            return 200
        }
    }
}
