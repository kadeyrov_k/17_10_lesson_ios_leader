//
//  CoffeeMachine.swift
//  17_10_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 17.10.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import Foundation

class CoffeeMachine {
    private(set) var milk: Int
    private(set) var coffee: Int
    private(set) var water: Int
    
    init(milk: Int, coffee: Int, water: Int) {
        self.milk = milk
        self.coffee = coffee
        self.water = water
    }
    
    func addCoffee(add: Int) {
        self.coffee += add
    }
    func addMilk(add: Int) {
        self.milk += add
    }
    func addWater(add: Int) {
        self.water += add
    }
    
    func getCoffee(coffee: Coffee) -> String {
        if self.coffee < coffee.coffeeNeed {
            return "Add more coffee"
        }
        if self.milk < coffee.milkNeed {
            return "Add more milk"
        }
        if self.water < coffee.waterNeed {
            return "Add more water"
        }
        
        self.water -= coffee.waterNeed
        self.coffee -= coffee.coffeeNeed
        self.milk -= coffee.milkNeed
        
        return "Your`s \(coffee.rawValue)"
    }
    
}
