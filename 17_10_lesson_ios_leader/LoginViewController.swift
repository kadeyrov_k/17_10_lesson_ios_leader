//
//  LoginViewController.swift
//  17_10_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 17.10.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var helloLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func okButtonTouchUpInside(_ sender: Any) {
        if let name = nameTextField.text, let surname = surnameTextField.text {
        
            if name.count > 0 && surname.count > 0 {
                helloLabel.text = "Hello, \(name) \(surname)"
                nameTextField.text = ""
                surnameTextField.text = ""
                nameTextField.endEditing(true)
                surnameTextField.endEditing(true)
            }
            else {
                helloLabel.text = "Set name and surname!!!"
                nameTextField.endEditing(true)
                surnameTextField.endEditing(true)
            }
        }
    }
    
}
