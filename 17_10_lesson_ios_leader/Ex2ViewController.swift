//
//  Ex2ViewController.swift
//  17_10_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 17.10.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

class Ex2ViewController: UIViewController {
    @IBOutlet weak var coffeeCapLabel: UILabel!
    @IBOutlet weak var waterCapLAbel: UILabel!
    @IBOutlet weak var milkCapLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    let coffeeMach = CoffeeMachine(milk: 100, coffee: 100, water: 100)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateCap()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func addCoffeeTouch(_ sender: Any) {
        coffeeMach.addCoffee(add: 100)
        updateCap()
    }
    @IBAction func addMilkTouch(_ sender: Any) {
        coffeeMach.addMilk(add: 100)
        updateCap()
    }
    @IBAction func addWaterTouch(_ sender: Any) {
        coffeeMach.addWater(add: 100)
        updateCap()
    }
    
    func updateCap() {
        waterCapLAbel.text = "\(coffeeMach.water)"
        milkCapLabel.text = "\(coffeeMach.milk)"
        coffeeCapLabel.text = "\(coffeeMach.coffee)"
    }
    
    
    @IBAction func LatteTouch(_ sender: Any) {
        infoLabel.text = coffeeMach.getCoffee(coffee: .latte)
        updateCap()
    }
    @IBAction func blackCoffeeTouch(_ sender: Any) {
        infoLabel.text = coffeeMach.getCoffee(coffee: .blackCoffee)
        updateCap()
    }
    @IBAction func bigCoffeeeTouch(_ sender: Any) {
        infoLabel.text = coffeeMach.getCoffee(coffee: .bigCoffee)
        updateCap()
    }
    
}
